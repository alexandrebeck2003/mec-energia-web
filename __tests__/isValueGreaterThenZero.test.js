import { isValueGreaterThenZero } from "../src/components/ConsumerUnit/Form/Create";
import "@testing-library/jest-dom";

describe("Testando isValueGreaterThenZero", () => {
  it("deve retornar mensagem de erro para valores não numéricos", () => {
    expect(isValueGreaterThenZero("texto")).toBe("Valor deve ser numérico");
  });

  it("deve retornar mensagem de erro para null", () => {
    expect(isValueGreaterThenZero(null)).toBe("Valor deve ser numérico");
  });

  it("deve retornar mensagem de erro para zero", () => {
    expect(isValueGreaterThenZero(0)).toBe("Insira um valor maior que 0");
  });

  it("deve retornar mensagem de erro para valores negativos", () => {
    expect(isValueGreaterThenZero(-5)).toBe("Insira um valor maior que 0");
  });

  it("deve retornar undefined para valores positivos", () => {
    expect(isValueGreaterThenZero(10)).toBeUndefined();
  });
});
